function fixAllReposts() {
	var elements = document.getElementsByTagName("A");
	for (var i = 0; i < elements.length; i++) {
		// Find reposts' post-time link which has the original post id
		var a = elements[i];
		if (!a.getAttribute("href") || !a.getAttribute("href").startsWith("/posts/"))
			continue;
		if (!a.parentElement || !a.parentElement.parentElement)
			continue;
		var div = a.parentElement.parentElement;
		if (!div.parentElement || !div.parentElement.parentElement)
			continue;
		div = div.parentElement.parentElement;
		if (!div.parentElement)
			continue;
		div = div.parentElement;
		if (div.className != "post-media-meta")
			continue;
		var id = a.getAttribute("href").substr(7);
		var divs = div.getElementsByClassName("post-text");
		if (!divs.length)
			continue;
		// Fix repost body id
		divs[0].setAttribute("data-id", id);
	}
}

window.addEventListener('load', function() {
	// Clear local storage that could be spammed by sharethis.com
	window.localStorage.clear();

	// Fix modal popup that scolls html body to top
	var elements = document.getElementsByTagName("STYLE");
	var toBeReplaced = "body.modal-open { position: fixed !important; }";
	for (var i = 0; i < elements.length; i++) {
		var css = elements[i].textContent;
		var index = css.indexOf(toBeReplaced);
		if (index != -1) {
			elements[i].textContent = css.substr(0, index) +
				"body.modal-open { position: inherit !important; }" +
				css.substr(index + toBeReplaced.length);
		}
	}

	// Fix repost id
	var divPosts = document.getElementsByClassName("js_posts_stream");
	if (divPosts.length) {
		divPosts = divPosts[0];
		var ul = divPosts.getElementsByTagName("UL");
		if (ul.length) {
			ul = ul[0];
			var observer = new MutationObserver(fixAllReposts);
			var observerConfig = { childList: true };
			observer.observe(ul, observerConfig);
		}
	}
	fixAllReposts();

	//alert("Guo.Media Fixer initialized");
}, false);
